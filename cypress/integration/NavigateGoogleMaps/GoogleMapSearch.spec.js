// Goolge Map Test Cypress Automation



describe('GoogleMapSearch', () => {

    it('Navigate to google', () => {
        cy.visit('https://www.google.com/maps/@-37.9074157,145.0445587,11.94z', { failOnStatusCode: false })

    })

    it('Search Bunnings', () => {

        cy.get('#searchboxinput')
            .type('Bunnings')
            .wait(5000)
    })

    it('Click Complete Address', () => {

        cy.get('#sbse4 > .sbqs_c > .ZHeE1b')
            .click()

    })

    it('Verify Proper Address Exist', () => {
        cy.wait(5000)
            .get(':nth-child(1) > .CsEnBe > .AeaXub > .rogA2c > .QSFF4-text')
            .should('have.text', '64-96 Gaffney St, Coburg VIC 3058, Australia');

    })

    it('Verify Direction Button Exist', () => {

        cy.get('.pChizd > .S9kvJb')
            .should('be.visible')

    })


    it('Verify Save Button Exist', () => {

        cy.get('[aria-label="Actions for Bunnings Coburg"] > :nth-child(2) > .S9kvJb')
            .should('be.visible')

    })

    it('Verify Nearby Button Exist', () => {

        cy.get(':nth-child(3) > .S9kvJb')
            .should('be.visible')

    })

    it('Verify STYP Button Exist', () => {

        cy.get(':nth-child(4) > .S9kvJb')
            .should('be.visible')

    })

    it('Verify Share Button Exist', () => {

        cy.get(':nth-child(5) > .S9kvJb')
            .should('be.visible')

    })

})